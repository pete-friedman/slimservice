ItemVO = function() {
	this.itemId = "";
	this.itemType = "";
	this.itemTitle = "";
	this.itemTagline = "";
	this.itemMessage= "";
	this.itemCity = "";
	this.itemState = "";
	this.itemCountry = "";
	this.itemUrl = "";
	this.lastModified = "";
	this.isApproved = "";
	this.isActive = "";
	this.contactName = "";
	this.contactEmail = "";
};