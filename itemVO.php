<?php
class itemVO {
	public $itemId = "";
	public $itemType = "";
	public $itemTitle = "";
	public $itemTagline = "";
	public $itemMessage= "";
	public $itemCity = "";
	public $itemState = "";
	public $itemCountry = "";
	public $itemUrl = "";
	public $lastModified = "";
	public $isApproved = "";
	public $isActive = "";
	public $contactName = "";
	public $contactEmail = "";
}
?>