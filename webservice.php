<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST,PUT, OPTIONS, DELETE');
header('Access-Control-Allow-Headers: Content-Type, Accept,Access-Control-Request-Method,Origin');

// remove for production
//error_reporting ( E_ALL );
//ini_set ( 'display_errors', TRUE );
//ini_set ( 'display_startup_errors', TRUE );

/**
 * slimService
 *
 * @author Pete Friedman <pete@petefriedman.com>
 */
class slimService
{

    public $modelId;
    public $dbh;
    public $app;

    private $image;
    private $datamodel;
    private static $instance;

    function __autoload()
    {
    }

    /**
     * enforces Singleton pattern
     */
    public static function getInstance()
    {
        if (!isset (self::$instance)) {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }

    function __construct()
    {
        require_once('itemVO.php');
        require('Slim/Slim.php');
        $dbhost = DB_HOST;
        $dbuser = DB_USER;
        $dbpass = DB_PASSW;
        $dbname = DB_NAME;
        $this->init();
    }

    private function init()
    {
        error_log("init");
        /**
         * init
         * - registers the Slim Framework and defines the webService
         * Application Route Table
         * - Rest method and verb mappings to php methods for the webService app are defined here.
         */
        \Slim\Slim::registerAutoloader();
        $this->app = new \Slim\Slim();

        /**
         * item
         */
        $this->app->get('/item/:itemId', function ($id) {
            return slimService::getItem($id);
        });

        $this->app->put('/item/:itemId', function ($id) {
            return slimService::updateItem($id);
        });


        $this->app->post('/item/:itemId', function ($id) {
            return slimService::addItem($id);
        });

        $this->app->delete('/delete/:itemId', function ($id) {
            return slimService::deleteItem($id);
        });

        /** approval of items */
        $this->app->put('/approve/:itemId', function ($id) {
            return slimService::toggleApproval($id, "true");
        });

        $this->app->put('/unapprove/:itemId', function ($id) {
            return slimService::toggleApproval($id, "false");
        });

        // get all data for admin view - approved items only
        $this->app->get('/all', function () {
            return slimService::getResults('all');
        });

        // get all data for admin view - approved items only
        $this->app->get('/unapproved', function () {
            return slimService::getResults('unapproved');
        });

        // get all data for admin view, approved and unapproved
        $this->app->get('/stations', function () {
            return slimService::getResults('stations');
        });

        // get all data for admin view, approved and unapproved
        $this->app->get('/cities', function () {
            return slimService::getResults('cities');
        });

        // get all data for admin view, approved and unapproved
        $this->app->get('/events', function () {
            return slimService::getResults('events');
        });
        // get all data for admin view, approved and unapproved
        $this->app->get('/recordstores', function () {
            return slimService::getResults('recordstores');
        });

        $this->app->get('/tally', function () {
            return slimService::getTally();
        });

        $this->app->post('/email/:rowid', function ($id) {
            return slimService::addEmail($id);

        });


        // Default route - does nothing by design.
        $this->app->get('/', function () {
        });

        $this->app->run();

    }

    /**
     * Get itemVO by $id
     *
     * @param id string
     * @return json object
     */
    public function getItem($id, $outputType = 'json')
    {

        $sql = "SELECT * FROM " . DB_NAME . ".events WHERE itemId='" . $id . "'";
        try {
            $db = self::getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("itemId", $id);
            $stmt->execute();
            $item = $stmt->fetchObject();
            $db = null;
        } catch (PDOException $e) {
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }

        // $jsnItem->type = (string)$item['type'];
        $obj = new itemVO();
        $obj->itemId = $item->itemId;
        $obj->itemType = $item->itemType;
        $obj->itemTitle = $item->itemTitle;
        $obj->itemTagline = $item->itemTagline;
        $obj->itemMessage = $item->itemMessage;
        $obj->itemCity = $item->itemCity;
        $obj->itemState = $item->itemState;
        $obj->itemCountry = $item->itemCountry;
        $obj->itemUrl = $item->itemUrl;
        $obj->lastModified = $item->lastModified;
        $obj->contactEmail = $item->contactEmail;
        $obj->contactName = $item->contactName;
        $obj->isActive = $item->isActive;
        $obj->isApproved = $item->isApproved;

        $retObj = new stdClass();
        $retObj->data = $obj;
        if ($outputType == 'object') {
            return $obj;
        } else {
            // error_log("SELECTING!".$wl);
            echo json_encode($retObj);
        };
    }


    /**
     * getResults
     *
     * @param id string
     * @return  object
     */
    public function getResults($command = 'all', $outputType = 'json')
    {

        $items = array();
        $retItems = array();
        $jsonObj = new stdClass();
        $typeClause = '';
        error_log("getResults:" . $command, 0);

        switch ($command) {

            case "all":
                $typeClause = '';
                break;
            case "stations":
            case "cities":
            case "events":
            case "recordstores":
                $typeClause = " itemType='" . $command . "' AND ";
                break;
        }


        switch ($command) {
            case "all":
                $sql = "SELECT * FROM " . DB_NAME . ".events  WHERE isApproved = 'true' AND isActive = 'true'";

                break;
            case "approved":
                $sql = "SELECT * FROM " . DB_NAME . ".events WHERE $typeClause  isApproved = 'true' AND isActive = 'true'";
                break;
            case "unapproved":
                $sql = "SELECT * FROM " . DB_NAME . ".events WHERE $typeClause isApproved = 'false' AND isActive = 'true'";
                break;
            default:
                $sql = "SELECT * FROM " . DB_NAME . ".events WHERE $typeClause  isApproved = 'true' AND isActive = 'true'";
        }
        error_log("GET RESULS SQL=" . $sql);
        try {
            $db = self::getConnection();
            $stmt = $db->query($sql);
            $items = $stmt->fetchAll();
            $db = null;
        } catch (PDOException $e) {
            echo '{"error":{"text":' . "oh oh " . $e->getMessage() . '}}';
        }

        foreach ($items as $row) {
            $obj = new stdClass();
            $obj->itemId = (string)$row['itemId'];
            $obj->itemType = (string)$row['itemType'];
            $obj->itemTitle = (string)$row['itemTitle'];
            $obj->itemTagline = (string)$row['itemTagline'];
            $obj->itemMessage = (string)$row['itemMessage'];
            $obj->itemCity = (string)$row['itemCity'];
            $obj->itemState = (string)$row['itemState'];
            $obj->itemCountry = (string)$row['itemCountry'];
            $obj->itemUrl = (string)$row['itemUrl'];
            $obj->contactName = (string)$row['contactName'];
            $obj->contactEmail = (string)$row['contactEmail'];
            $obj->lastModified = (string)$row ['lastModified'];
            $obj->isApproved = (string)$row ['isApproved'];
            $obj->isActive = (string)$row ['isActive'];
            array_push($retItems, $obj);
        }
        $jsonObj->data = $retItems;


        if ($outputType == 'object') {
            return $jsonObj;
        } else {
            echo json_encode($jsonObj);
        };
    }

    public function addEmail($id)
    {

        $request = \Slim\Slim::getInstance()->request();
        $item = json_decode($request->getBody());

        /*$obj = new stdClass();
        $obj->rowid = (string)$item['rowid'];
        $obj->firstname = (string)$item['firstname'];
        $obj->lastname = (string)$item['lastname'];
        $obj->email = (string)$item['email'];*/

        $sql = "INSERT INTO " . DB_NAME . ".emails ( rowid, firstname, lastname, email )";
        $sql .= " VALUES(:rowid, :firstname, :lastname, :email)";

        try {
            $db = self::getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("rowid", $item->rowid);
            $stmt->bindParam("firstname", $item->firstname);
            $stmt->bindParam("lastname", $item->lastname);
            $stmt->bindParam("email", $item->email);

            $stmt->execute();
            $db = null;
        } catch (PDOException $e) {

            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        header('Content-Type: application/json');
        header('Status: 200');
        echo json_encode($item);

    }

    /**
     * Adds an entry
     *
     * @param id
     * @return json object
     */
    public function addItem($id)
    {

        $request = \Slim\Slim::getInstance()->request();
        $item = json_decode($request->getBody());

        $obj = new itemVO();
        $obj->itemId = $item->itemId;
        $obj->itemType = $item->itemType;
        $obj->itemTitle = $item->itemTitle;
        $obj->itemTagline = $item->itemTagline;
        $obj->itemMessage = $item->itemMessage;
        $obj->itemCity = $item->itemCity;
        $obj->itemState = $item->itemState;
        $obj->itemCountry = $item->itemCountry;
        $obj->itemUrl = $item->itemUrl;
        $obj->contactName = $item->contactName;
        $obj->contactEmail = $item->contactEmail;

        error_log("half stack?");

        $sql = "INSERT INTO " . DB_NAME . ".events ( itemId, itemType, itemTitle, itemTagline, itemMessage,";
        $sql .= "itemCity,itemState,itemCountry,itemUrl,contactName,contactEmail ) VALUES";
        $sql .= "( :itemId, :itemType, :itemTitle,:itemTagline, :itemMessage,:itemCity,:itemState,";
        $sql .= ":itemCountry,:itemUrl,:contactName,:contactEmail)";

        error_log("SQL=" . $sql);

        try {
            $db = self::getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("itemId", $item->itemId);
            $stmt->bindParam("itemType", $item->itemType);
            $stmt->bindParam("itemTitle", $item->itemTitle);
            $stmt->bindParam("itemTagline", $item->itemTagline);
            $stmt->bindParam("itemMessage", $item->itemMessage);
            $stmt->bindParam("itemCity", $item->itemCity);
            $stmt->bindParam("itemState", $item->itemState);
            $stmt->bindParam("itemCountry", $item->itemCountry);
            $stmt->bindParam("itemUrl", $item->itemUrl);
            $stmt->bindParam("contactName", $item->contactName);
            $stmt->bindParam("contactEmail", $item->contactEmail);

            $stmt->execute();
            $db = null;
        } catch (PDOException $e) {
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        header('Content-Type: application/json');
        header('Status: 200');
        echo json_encode($item);
    }


    /**
     * Updates an item
     * @param id: string
     * @return
     */
    public function updateItem($id, $outputType = 'json')
    {
        error_log("UPDATE ITEM");
        $request = \Slim\Slim::getInstance()->request();
        $item = json_decode($request->getBody());
        $thing = $request->getBody();
        error_log("incoming item=" . json_encode($item));
        $sql = "UPDATE " . DB_NAME . ".events SET itemType=:itemType, itemTitle=:itemTitle,";
        $sql .= " itemTagline =:itemTagline, itemMessage =:itemMessage, itemCity =:itemCity,";
        $sql .= " itemState =:itemState, itemCountry =:itemCountry, itemUrl =:itemUrl,";
        $sql .= " contactName =:contactName, contactEmail =:contactEmail,";
        $sql .= " isActive =:isActive, isApproved =:isApproved WHERE itemId='" . $id . "'";

        error_log("SQL=" . $sql);

        try {
            $db = self::getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("itemType", $item->itemType);
            $stmt->bindParam("itemTitle", $item->itemTitle);
            $stmt->bindParam("itemTagline", $item->itemTagline);
            $stmt->bindParam("itemMessage", $item->itemMessage);
            $stmt->bindParam("itemCity", $item->itemCity);
            $stmt->bindParam("itemState", $item->itemState);
            $stmt->bindParam("itemCountry", $item->itemCountry);
            $stmt->bindParam("itemUrl", $item->itemUrl);
            $stmt->bindParam("contactName", $item->contactName);
            $stmt->bindParam("contactEmail", $item->contactEmail);
            $stmt->bindParam("isApproved", $item->isApproved);
            $stmt->bindParam("isActive", $item->isActive);
            $stmt->execute();
            $db = null;
        } catch (PDOException $e) {
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        echo $thing;

    }

    /**
     * Deletes an entry by setting isActive to false
     *
     * @param $id
     * @return jsonObject
     */
    public function deleteItem($id)
    {
        error_log("delete item");

        $sql = "UPDATE " . DB_NAME . ".events SET isActive='false', isApproved='false' WHERE itemId=:itemId";
        try {
            $db = self::getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("itemId", $id);
            $stmt->execute();
            $db = null;
        } catch (PDOException $e) {
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        echo('{"command":"delete","status":"200"}');
    }

    /**
     * Approve / unapprove an item
     *
     * @param id string
     * @param command string
     * @return
     */
    public function toggleApproval($id, $command = "true")
    {
        $request = \Slim\Slim::getInstance()->request();
        $item = json_decode($request->getBody());
        $sql = "UPDATE " . DB_NAME . ".events SET isApproved='" . $command . "' WHERE itemId=:itemId";
        error_log("sql1=" . $sql);
        try {
            $db = self::getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("itemId", $id);
            $stmt->execute();
            $db = null;
        } catch (PDOException $e) {
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        echo json_encode($item);
    }


    /**
     * getTally
     *
     * returns an object containing the count of the itemTypes
     */
    public function getTally()
    {

        $request = \Slim\Slim::getInstance()->request();
        $item = json_decode($request->getBody());
        $sql = "SELECT itemType,COUNT(*) AS counter FROM " . DB_NAME . ".events WHERE isApproved='true' AND isActive='true' GROUP BY itemType ";

        try {
            $db = self::getConnection();
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $items = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
        } catch (PDOException $e) {
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        echo json_encode($items);
    }


    /**
     * Returns the DB connection
     *
     * @return DB connection Object
     */
    public function getConnection()
    {
        $dbh = new PDO ("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSW);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbh;
    }


}

// --------------------------------------------- config

require_once('appConfig.php');
$service = slimService::getInstance();
?>