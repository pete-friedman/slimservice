<?php

namespace slimService;


// remove for production
/*
error_reporting ( E_ALL );
ini_set ( 'display_errors', 'on' );
ini_set ( 'display_startup_errors', 'on' );
*/


switch (gethostname ()) {
	case $_ENV.APP_HOST :
		define ( 'DB_HOST', 'localhost:3306' );
		define ( 'DB_USER',  $_ENV.DB_USER );
		define ( 'DB_NAME', $_ENV.DB_NAME );
		define ( 'DB_PASSW',  $_ENV.DB_PASSW );
		define ( 'SERVICE_URL', "https://dev.sites/pfiedman/service/service.php" );
		define ( 'APP_BASE', "https://dev.sites/pfriedman/dist" );
	break;
	
	default :
		define ( 'DB_HOST', 'localhost' );
        define ( 'DB_USER',  $_ENV.DB_USER );
        define ( 'DB_NAME', $_ENV.DB_NAME );
        define ( 'DB_PASSW',  $_ENV.DB_PASSW );
		define ( 'SERVICE_URL', "https://www.petefriedman.com/service/service.php" );
		define ( 'APP_BASE', "https://www.petefriedman.com/" );
}
?>